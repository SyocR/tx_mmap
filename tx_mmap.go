package tx_mmap

import (
	"fmt"
	"golang.org/x/sys/unix"
	"net"
	"reflect"
	"runtime"
	"unsafe"
)

/*
#include <linux/if_packet.h>  // AF_PACKET, sockaddr_ll
#include <linux/if_ether.h>  // ETH_P_ALL
#include <sys/socket.h>  // socket()
#include <unistd.h>  // close()
#include <arpa/inet.h>  // htons()
#include <sys/mman.h>  // mmap(), munmap()
#include <poll.h>  // poll()
#include <linux/if_ether.h>
int Sendtosocket(int fd, int blocking) {
	return sendto(fd,
        NULL,
		  0,
		  (blocking? 0 : MSG_DONTWAIT),
        NULL,
        0);
}
*/
import "C"

type TX_Mmap struct {
	// fd is the C file descriptor.
	fd int
	// ring points to the memory space of the ring buffer shared by tpacket and the kernel.
	ring []byte
	// opts contains read-only options for the TX_Mmap object.
	opts options
	// rawring is the unsafe pointer that we use to poll for packets
	rawring unsafe.Pointer
	// offset is the offset into the ring of the current header.
	offset int
	// current is the current header.
	current header
	// Hackity hack hack hack.  We need to return a pointer to the header with
	// getMmapHeader, and we don't want to allocate a v3wrapper every time,
	// so we leave it in the TX_Mmap object and return a pointer to it.
	v3 v3wrapper
}

// bindToInterface binds the TX_Mmap socket to a particular named interface.
func (h *TX_Mmap) bindToInterface(ifaceName string) error {
	ifIndex := 0
	// An empty string here means to listen to all interfaces
	if ifaceName != "" {
		iface, err := net.InterfaceByName(ifaceName)
		if err != nil {
			return fmt.Errorf("InterfaceByName: %v", err)
		}
		ifIndex = iface.Index
	}
	s := &unix.SockaddrLinklayer{
		Protocol: htons(uint16(unix.ETH_P_ALL)),
		Ifindex:  ifIndex,
	}
	return unix.Bind(h.fd, s)
}

// setUpRing sets up the shared-memory ring buffer between the user process and the kernel.
func (h *TX_Mmap) setUpRing() (err error) {
	numFrames := h.opts.numFrames
	frameSize := h.opts.frameSize
	totalSize := int(numFrames * frameSize)

	var tp C.struct_tpacket_req3
	tp.tp_frame_size = C.uint(frameSize)
	tp.tp_block_size = C.uint(frameSize)
	tp.tp_block_nr = C.uint(numFrames)
	tp.tp_frame_nr = C.uint(numFrames)
	//fmt.Println("frame & blocksize", tp.tp_frame_size, "block & framenr", tp.tp_frame_nr)

	if err := setsockopt(h.fd, unix.SOL_PACKET, unix.PACKET_TX_RING, unsafe.Pointer(&tp), unsafe.Sizeof(tp)); err != nil {
		return fmt.Errorf("setsockopt packet_rx_ring v3: %v", err)
	}
	// TODO make optional
	// Bypass kernel qdisk (traffic control) layer
	val := C.int(1)
	if err := setsockopt(h.fd, unix.SOL_PACKET, unix.PACKET_QDISC_BYPASS, unsafe.Pointer(&val), unsafe.Sizeof(val)); err != nil {
		return fmt.Errorf("setsockopt packet_rx_ring v3: %v", err)
	}
	// TODO add LOSS socket option
	h.ring, err = unix.Mmap(h.fd, 0, totalSize, unix.PROT_READ|unix.PROT_WRITE, unix.MAP_SHARED)
	if err != nil {
		return err
	}
	if h.ring == nil {
		return fmt.Errorf("no ring")
	}
	h.rawring = unsafe.Pointer(&h.ring[0])
	return nil
}

// Close cleans up the TX_Mmap.  It should not be used after the Close call.
func (h *TX_Mmap) Close() {
	h.Send() // ensure packets are sent

	if h.fd == -1 {
		return // already closed.
	}
	if h.ring != nil {
		unix.Munmap(h.ring)
	}
	h.ring = nil
	unix.Close(h.fd)
	h.fd = -1
	runtime.SetFinalizer(h, nil)
}

// NewTX_Mmap returns a new TX_Mmap object for writing packets to the wire.
// Its behavior may be modified by passing in any/all of tx_mmap.Opt* to this
// function.
// If this function succeeds, the user should be sure to Close the returned
// TX_Mmap when finished with it.
func NewTX_Mmap(opts ...interface{}) (h *TX_Mmap, err error) {
	h = &TX_Mmap{}
	fd, err := unix.Socket(unix.AF_PACKET, unix.SOCK_RAW, int(htons(unix.ETH_P_ALL)))
	if err != nil {
		return nil, err
	}
	if h.opts, err = parseOptions(opts...); err != nil {
		return nil, err
	}
	h.fd = fd
	if err = h.bindToInterface(h.opts.iface); err != nil {
		h.Close()
		return nil, err
	}
	if err = h.setUpRing(); err != nil {
		h.Close()
		return nil, err
	}
	runtime.SetFinalizer(h, (*TX_Mmap).Close)
	return h, nil
}

// Send message to socket telling kernel to send packets in buffer.
// Exposed to allow user to send packets as they wish
func (h *TX_Mmap) Send() int {
	// Was only able to get it working by inlining C
	return int(C.Sendtosocket(C.int(h.fd), h.opts.blocking))
}

// Loop through the mmap looking for available frames.
// Copy data into available frame and update status
func (h *TX_Mmap) WritePacketData(data []byte) (written int, err error) {
	for {
		var status int
		h.current, status = h.getMmapHeader()
		switch status {
		case unix.TP_STATUS_AVAILABLE:
			written := h.current.writeData(data)
			return written, nil
		case unix.TP_STATUS_SENDING, unix.TP_STATUS_SEND_REQUEST:
			if h.opts.userSends {
				runtime.Gosched() // thight loop, yield
			} else {
				h.Send()
			}
		case unix.TP_STATUS_WRONG_FORMAT:
			return 0, fmt.Errorf("Found frame with TP_STATUS_WRONG_FORMAT")
		default:
			return 0, fmt.Errorf("Got unknown status code %d", status)
		}
	}
}

// Return a pointer to where the next packet should be written in the mmap
// as well as pointers to the TP_STATUS and TP_LENGTH flags.
func (h *TX_Mmap) GetMmapBuffer() (data unsafe.Pointer, flagAvail unsafe.Pointer, flagLen unsafe.Pointer, err error) {
	for {
		var status int
		h.current, status = h.getMmapHeader()
		switch status {
		case unix.TP_STATUS_AVAILABLE:
			data, flagAvail, flagLen = h.current.getPointers()
			return data, flagAvail, flagLen, nil
		case unix.TP_STATUS_SENDING, unix.TP_STATUS_SEND_REQUEST:
			if h.opts.userSends {
				runtime.Gosched() // thight loop, yield
			} else {
				h.Send()
			}
		case unix.TP_STATUS_WRONG_FORMAT:
			data, _, _ = h.current.getPointers()
			return nil, nil, nil, fmt.Errorf("Found frame with TP_STATUS_WRONG_FORMAT at %p", data)
		default:
			return nil, nil, nil, fmt.Errorf("Got unknown status code %d", status)
		}
	}
}

func (h *TX_Mmap) getMmapHeader() (header, int) {
	if h.offset >= h.opts.numFrames {
		// we have reached the end of the mmap
		// start at the beginning again
		h.offset = 0
	}
	position := uintptr(h.rawring) + uintptr(h.opts.frameSize*h.offset)
	h.v3 = initV3Wrapper(unsafe.Pointer(position))
	h.offset++
	return &h.v3, int(h.v3.framehdr.tp_status)
}

type header interface {
	// writeData writes the data to the packet offset
	writeData([]byte) int
	// gets a pointer for writing data and setting the status flag
	getPointers() (unsafe.Pointer, unsafe.Pointer, unsafe.Pointer)
}

// Creates a slice for the data so we can use copy on it later
func makeSlice(start uintptr, length int) (data []byte) {
	slice := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	slice.Data = start
	slice.Len = length
	slice.Cap = length
	return
}

type v3wrapper struct {
	framehdr *C.struct_tpacket_hdr
	// pointer to where we start writing data to be sent
	packet unsafe.Pointer
}

// Get pointers for a new segment in the mmap
func initV3Wrapper(block unsafe.Pointer) (w v3wrapper) {
	w.framehdr = (*C.struct_tpacket_hdr)(block)
	offset := uintptr(unix.TPACKET_HDRLEN - unsafe.Sizeof(unix.RawSockaddrLinklayer{}))
	w.packet = unsafe.Pointer(uintptr(unsafe.Pointer(w.framehdr)) + offset)
	return
}

// Create a slice in the mmap buffer for our data and copy our data into it.
// Set the status flag to SEND_REQUEST so the frame will be sent on the next
// sendto call to the socket
func (w *v3wrapper) writeData(data []byte) int {
	bufSlice := makeSlice(uintptr(unsafe.Pointer(w.packet)), int(len(data)))

	w.framehdr.tp_len = C.uint(len(data))
	w.framehdr.tp_status = unix.TP_STATUS_SEND_REQUEST

	written := copy(bufSlice, data)

	return written
}

func (w *v3wrapper) getPointers() (data unsafe.Pointer, tp_status unsafe.Pointer, tp_len unsafe.Pointer) {
	data = unsafe.Pointer(w.packet)
	tp_status = unsafe.Pointer(&w.framehdr.tp_status)
	tp_len = unsafe.Pointer(&w.framehdr.tp_len)
	return
}

// setsockopt provides access to the setsockopt syscall.
func setsockopt(fd, level, name int, val unsafe.Pointer, vallen uintptr) error {
	_, _, errno := unix.Syscall6(
		unix.SYS_SETSOCKOPT,
		uintptr(fd),
		uintptr(level),
		uintptr(name),
		uintptr(val),
		vallen,
		0,
	)
	if errno != 0 {
		return error(errno)
	}

	return nil
}

// htons converts a short (uint16) from host-to-network byte order.
// Thanks to mikioh for this neat trick:
// https://github.com/mikioh/-stdyng/blob/master/afpacket.go
func htons(i uint16) uint16 {
	//fmt.Println("Custom htons")
	return (i<<8)&0xff00 | i>>8
}
