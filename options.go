// Copyright 2012 Google, Inc. All rights reserved.
//
// Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file in the root of the source
// tree.

// +build linux

package tx_mmap

import (
	"C"
	"errors"
)

// OptInterface is the specific interface to bind to.
// It can be passed into NewTPacket.
type OptInterface string

// OptFrameSize is TPacket's tp_frame_size
// It can be passed into NewTPacket.
type OptFrameSize int

// OptBlocking decides if sendto is a blocking operation
// It can be passed into NewTPacket.
type OptBlocking bool

// OptUserSends makes the user responsible for sending packets
// It can be passed into NewTPacket.
type OptUserSends bool

// OptNumFrames is TPacket's tp_frame_nr
// It can be passed into NewTPacket.
type OptNumFrames int

// Default constants used by options.
const (
	DefaultFrameSize = 1024 * 8 // Default value for OptFrameSize.
	DefaultNumFrames = 1024     // Default value for OptNumBlocks.
	DefaultBlocking  = C.int(1)
	DefaultUserSends = false
)

type options struct {
	frameSize int
	numFrames int
	iface     string
	blocking  C.int
	userSends bool
}

var defaultOpts = options{
	frameSize: DefaultFrameSize,
	numFrames: DefaultNumFrames,
	blocking:  DefaultBlocking,
	userSends: DefaultUserSends,
}

func parseOptions(opts ...interface{}) (ret options, err error) {
	ret = defaultOpts
	for _, opt := range opts {
		switch v := opt.(type) {
		case OptFrameSize:
			ret.frameSize = int(v)
		case OptNumFrames:
			ret.numFrames = int(v)
		case OptInterface:
			ret.iface = string(v)
		case OptBlocking:
			// Allows us to use this value directly in a more performance
			// critical code path later
			if v {
				ret.blocking = C.int(1)
			} else {
				ret.blocking = C.int(0)
			}
		case OptUserSends:
			ret.userSends = bool(v)
		default:
			err = errors.New("unknown type in options")
			return
		}
	}
	return
}
