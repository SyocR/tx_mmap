# tx_mmap

A simple library for sending packets using the AF_PACKET socket and the PACKET_TX_RING.  
This is an attempt at optimizing around TX speed.  
It is heavily based on [gopacket/afpacket](https://github.com/google/gopacket/tree/master/afpacket) though a lot of features are removed.  
You can only send using TX_RING with this library. People wanting RX or sending with `sendto` should use `gopacket/afpacket`.

## Options

OptInterface is the specific interface to bind to.
This is a required parameter.

OptFrameSize is tpacket tp_frame_size

OptNumFrames is tpacket tp_frame_nr  
Note that the memory required for the mmap ring is OptFrameSize * OptNumFrames bytes.

OptBlocking decides if sendto is a blocking operation  
Makes most sense in combination with OptUserSends

OptUserSends makes the user responsible for sending packets
See examples section.

## Examples

```
import "gitlab.com/SyocR/tx_mmap"

ifaceOpt := tx_mmap.OptInterface(iface)
numFramesOpt := tx_mmap.OptNumFrames(1024 * 50) 
tpacket, _ := tx_mmap.NewTX_Mmap(ifaceOpt, userSendsOpt, numFramesOpt)

tpacket.ZCWritePacketData([]byte{})
```

Simple example on how to send from userspace if OptUserSends is set to true.  

```
func afpacketSend(tpacket *tx_mmap.TX_Mmap) {
    for {
        sent := tpacket.Send()
        if sent == 0 {
            time.Sleep(time.Millisecond)
        }
    }
}
```

## TODO list

* Improve code quality and performance
* Tests
* Enable concurrency in the same TX_RING
* Try to remove inline C code